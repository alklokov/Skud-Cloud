﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SkudCloud.Data;
using SkudCloud.Data.Entities;

namespace SkudCloud.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperatorRoleController : ControllerBase
    {
        private readonly SkudDbContext _context;

        public OperatorRoleController(SkudDbContext context)
        {
            _context = context;
        }

        // GET: api/OperatorRole
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OperatorRole>>> GetOperatorRoles()
        {
            return await _context.OperatorRoles.ToListAsync();
        }

        // GET: api/OperatorRole/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OperatorRole>> GetOperatorRole(int id)
        {
            var operatorRole = await _context.OperatorRoles.FindAsync(id);

            if (operatorRole == null)
            {
                return NotFound();
            }

            return operatorRole;
        }

        // PUT: api/OperatorRole/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOperatorRole(int id, OperatorRole operatorRole)
        {
            if (id != operatorRole.Id)
            {
                return BadRequest();
            }

            _context.Entry(operatorRole).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OperatorRoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OperatorRole
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<OperatorRole>> PostOperatorRole(OperatorRole operatorRole)
        {
            _context.OperatorRoles.Add(operatorRole);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOperatorRole", new { id = operatorRole.Id }, operatorRole);
        }

        // DELETE: api/OperatorRole/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOperatorRole(int id)
        {
            var operatorRole = await _context.OperatorRoles.FindAsync(id);
            if (operatorRole == null)
            {
                return NotFound();
            }

            _context.OperatorRoles.Remove(operatorRole);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OperatorRoleExists(int id)
        {
            return _context.OperatorRoles.Any(e => e.Id == id);
        }
    }
}
