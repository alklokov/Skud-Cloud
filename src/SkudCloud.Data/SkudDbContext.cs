﻿using Microsoft.EntityFrameworkCore;
using SkudCloud.Data.Entities;

namespace SkudCloud.Data
{
    public class SkudDbContext : DbContext
    {
        public SkudDbContext(DbContextOptions<SkudDbContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<OperatorRole> OperatorRoles { get; set; }
        public DbSet<Operator> Operators { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OperatorRole>().HasData(
                new OperatorRole[]
                {
                    new OperatorRole {Id = 1, RoleName = "administrator", Description = "Администратор"},
                    new OperatorRole {Id = 2, RoleName = "installer", Description = "Инсталлятор"},
                    new OperatorRole {Id = 3, RoleName = "buro", Description = "Оператор бюро пропусков"},
                    new OperatorRole {Id = 4, RoleName = "security", Description = "Охранник"},
                    new OperatorRole {Id = 5, RoleName = "subscriber", Description = "Подписчик"}
                }
            );

            modelBuilder.Entity<Operator>()
                .HasOne(o => o.OperatorRole)
                .WithMany()
                .IsRequired();
        }
    }
}
