﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace SkudCloud.Data.Entities
{
    /// <summary>
    /// Операторы системы
    /// </summary>
    [Index("Login", IsUnique = true)]
    public class Operator
    {
        /// <summary>
        /// id оператора в системе
        /// </summary>
        public int Id { get; set; }

        [Required()]
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        //public int OperatorRoleId { get; set; }
        public OperatorRole OperatorRole { get; set; }

    }
}
