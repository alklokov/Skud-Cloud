﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SkudCloud.Data.Entities
{
    /// <summary>
    /// Роли операторов системы
    /// </summary>
    public class OperatorRole
    {
        /// <summary>
        /// Id роли в системе
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Краткое наименование роли
        /// </summary>
        [Required]
        public string RoleName { get; set; }
        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }

        //public IEnumerable<Operator> Operators { get; set; }
    }
}
